import csv
import json

def reducepoints(array, ratio):
    # to reduce by ratio, sample every 1/ratio points
    n = 1/ratio
    if n < 1:
        n = 1
    else:
        n = int(n)
    for i,value in enumerate(array):
        if i % n == 0:
            yield value
class NumericData:
    '''
    A single numeric data feed.
    Defines methods for:
    - loading from csv (by key)
    - Getting data since a date.
    - Getting mean,min,max etc of data since a date.

    Contains a location (lat/lng).
    '''
    def __init__(self, sensor, unit=None):
        self.sensor = sensor
        self.data = []
        self.unit = unit
    def add_datapoint(self, datetime, value, filter_invalid=True):
        if filter_invalid and value < 0:
            return False
        self.data.append((datetime, value))
        return True
    def __repr__(self):
        return '({},{},{} elements)'.format(self.sensor.lat, self.sensor.lng, len(self.data))
    def render_dict(self, num_points=None):
        data = self.data
        if num_points is not None and len(data) > num_points:
            # cut down the number of points
            # data = {key: list(reducepoints(value, num_points/len(data))) for key, value in data.items()}
            data = list(reducepoints(data, num_points/len(data)))
        return {
            'lat': self.sensor.lat,
            'lng': self.sensor.lng,
            'unit': self.unit,
            'data': data
        }
