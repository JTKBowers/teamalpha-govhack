'''
Contains the abstract base class for a dataset.
'''

from abc import ABCMeta, abstractmethod

class Dataset(metaclass=ABCMeta):
    @abstractmethod
    def get_sensors(self):
        '''
        Return a dictionary of sensors, where each item follows the format
        {
            'name': '...',
            'lat': ...,
            'lng': ...
            'provides': [temperature, pressure,...]
        }
        '''
        pass
    @abstractmethod
    def get_feed(self, feed_key):
        '''
        Return a dict of the format
        {
            "sensor_id": NumericData,
            ...
        }
        or None if it does not exist.
        '''
        pass
    def __add__(self, other):
        return DatasetPair(self, other)

class DatasetPair(Dataset):
    def __init__(self, a, b):
        self.a = a
        self.b = b
        self.data = dict(self.a.data, **self.b.data)
    def get_sensors(self):
        '''
        Return the union of the two sensor dictionaries.
        '''
        return dict(self.a.get_sensors(), **self.b.get_sensors())
    def get_feed(self, feed_key):
        '''
        Return the union of the two sensor dictionaries.
        '''
        feed_a = self.a.get_feed(feed_key)
        feed_b = self.b.get_feed(feed_key)
        if feed_a is not None and feed_b is not None:
            raise Exception("Key {} is not unique! {}|{}".format(feed_key, feed_a, feed_b))
        if feed_a is not None:
            return feed_a
        if feed_b is not None:
            return feed_b
        return None

class Sensor:
    def __init__(self, id, name, lat, lng, dataset_description, supports=[]):
        self.id = id
        self.name = name
        self.lat = lat
        self.lng = lng
        self.dataset_description = dataset_description
        self.supports = set(supports)
    def to_dict(self):
        return {
            'lat': self.lat,
            'lng': self.lng,
            'name': self.name,
            'dataset_description': self.dataset_description,
            'supports': list(self.supports)
        }
    def __repr__(self):
        return str(self.to_dict())
