import csv
from Dataset import Sensor

dataset_description = '<p>A dataset collected by environment canterbury, giving hourly air quality across a variety of sites.</p><a href="http://data.ecan.govt.nz/Catalogue/Method?MethodId=94">Source</a>'

def load_sensors(csv_path = 'data_feeds/airquality/data/stations.csv'):
    sensor_locations = {}
    with open(csv_path) as locationfile:
        csvreader = csv.reader(locationfile)
        keys = next(csvreader)

        # Raise an error if the keys aren't what we expect
        assert(keys == ['SiteNo','SiteName','LongName','D_S_Sites_Key','AirShed','lat','lng'])
        for row in csvreader:
            SiteNo, SiteName, LongName, D_S_Sites_Key, AirShed, lat, lng = row
            sensor_id = SiteNo
            sensor_locations[sensor_id] = Sensor(sensor_id,SiteName,lat,lng, dataset_description)
    return sensor_locations
