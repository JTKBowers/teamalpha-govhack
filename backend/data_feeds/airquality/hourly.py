import os, csv
from datetime import datetime

import NumericData
from Dataset import Dataset, Sensor
import data_feeds.airquality.sensors

def loadData(sensors, sensor_values_csv_path):
    '''
    Return a dict of {'datatype': {kiteid: numericData}}.
    '''

    sensor_values = {}

    for hourly_csv in os.listdir(sensor_values_csv_path):
        if not hourly_csv.endswith('.csv'):
            continue
        full_path = os.path.join(sensor_values_csv_path, hourly_csv)
        with open(full_path) as csvfile:
            csvreader = csv.reader(csvfile)
            keys = next(csvreader)

            # Raise an error if the keys aren't what we expect
            fieldnames = {
              'CO (mg/m3)': 'CO',
              'PM10 (ug/m3)':  'PM10',
              'PM2.5 (ug/m3)': 'PM2.5',
              'PMCoarse (ug/m3)': 'PMCoarse',
              'Relative humidity (%)': 'Relative humidity',
              'SO2 (ug/m3)': 'SO2',
              'Temperature 2m (DegC)': 'Temperature 2m',
              'Temperature (near ground - top of mast) (DegC)': 'Temperature (near ground - top of mast)',
              'Temperature 6m (DegC)': 'Temperature 6m',
              'Wind direction (Deg)': 'Wind direction',
              'Wind maximum (m/s)': 'Wind maximum',
              'Wind speed (m/s)': 'Wind speed'
            }
            units = {
              'CO (mg/m3)': 'mg/m³',
              'PM10 (ug/m3)':  'µg/m³',
              'PM2.5 (ug/m3)': 'µg/m³',
              'PMCoarse (ug/m3)': 'µg/m³',
              'Relative humidity (%)': '%',
              'SO2 (ug/m3)': 'µg/m³',
              'Temperature 2m (DegC)': '°C',
              'Temperature (near ground - top of mast) (DegC)': '°C',
              'Temperature 6m (DegC)': '°C',
              'Wind direction (Deg)': '°',
              'Wind maximum (m/s)': 'm/s',
              'Wind speed (m/s)': 'm/s'
            }
            for row in csvreader:
                if len(row) == 0:
                    continue
                localtime, station_name, *measurements = row

                found_key = False
                for key in sensors:
                    if sensors[key].name == station_name:
                        station_id = key
                        found_key = True
                        break
                if not found_key:
                    raise Exception('Could not find id of "{}"'.format(station_name))

                # coerce the datetime into the appropriate datatype
                localtime = localtime.replace('a.m.', 'AM')
                localtime = localtime.replace('p.m.', 'PM')
                localtime = datetime.strptime(localtime, "%d/%m/%Y %I:%M:%S %p")

                sensor = sensors[station_id]

                for value, key in zip(measurements, keys[2:]):
                    unit = units[key]
                    field = fieldnames[key]
                    if len(value) == 0:
                        continue
                    #print(field+':', value,unit)
                    sensor_values[field] = sensor_values.get(field, {})
                    if station_id not in sensor_values[field]:
                        sensor_values[field][station_id] = NumericData.NumericData(sensor, unit)
                    if sensor_values[field][station_id].add_datapoint(localtime, float(value)):
                        sensor.supports.add(field)


    return sensor_values


class HourlyAirQuality(Dataset):
    def __init__(self, sensor_location_csv_path='data_feeds/airquality/data/stations.csv', sensor_values_csv_path='data_feeds/airquality/data/hourly'):
        self.sensors = data_feeds.airquality.sensors.load_sensors(sensor_location_csv_path)
        self.data = loadData(self.sensors, sensor_values_csv_path)
    def get_sensors(self):
        return {key: value for key, value in self.sensors.items() if len(value.supports) > 0}
    def get_feed(self, feed_key):
        return self.data.get(feed_key, None)
