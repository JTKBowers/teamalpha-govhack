import requests

station_ids = [7,2,1,3,9,4,5,10,11]

for sid in station_ids:
    url = 'http://data.ecan.govt.nz/data/94/Air/Air%20quality%20data%20for%20a%20monitored%20site%20(Hourly)/CSV?SiteId='+str(sid)+'&StartDate=01%2F07%2F2016&EndDate=02%2F07%2F2016'
    #print(url)
    data = requests.get(url).text
    if '<' in data:
        print('error!')
        break

    print(data)
    station_name = sid
    with open('hourly'+str(sid)+'.csv', 'w') as f:
        f.write(data)
