#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
from datetime import datetime

import NumericData
from Dataset import Dataset, Sensor

dataset_description = '<p>A simple dataset provided by the ccc, occasionally collected by kites. It provides information on air quality, noise and brightness.</p><a href="https://github.com/ccc-digital-channels/datasets/tree/master/GovHack2016-Christchurch/Bounty-Data/Best%20Chch%20Smart%20City%20Hack%20and%20Flood%20Prone%20Areas%20Hack">Source</a>'


def load_sensors(sensor_location_csv_path):
    sensor_locations = {}
    with open(sensor_location_csv_path) as locationfile:
        csvreader = csv.reader(locationfile)
        keys = next(csvreader)

        # Raise an error if the keys aren't what we expect
        assert(keys == ['kite_id', 'name', 'latitude', 'longitude'])
        for row in csvreader:
            kiteid, name, lat, lng = row
            sensor_locations[kiteid] = Sensor(kiteid,name,lat,lng, dataset_description)
    return sensor_locations

def loadData(sensors, sensor_values_csv_path):
    '''
    Return a dict of {'datatype': {kiteid: numericData}}.
    '''

    sensor_values = {
        'temperature': {},
        'humidity': {},
        'pressure': {},
        'luminosity': {},
        'co2': {},
        'sound': {}
    }

    temperatures = sensor_values['temperature']
    humidities = sensor_values['humidity']
    pressures = sensor_values['pressure']
    luminosities = sensor_values['luminosity']
    co2_values = sensor_values['co2']
    sound_values = sensor_values['sound']

    with open(sensor_values_csv_path) as csvfile:
        csvreader = csv.reader(csvfile)
        keys = next(csvreader)
        # Raise an error if the keys aren't what we expect
        assert(keys == ['localtime', ' kiteid', ' temperature', ' humidity', ' pressure', ' luminosity', ' co2', ' sound'])
        for row in csvreader:
            localtime, kiteid, temperature, humidity, pressure, luminosity, co2, sound = row

            # coerce the datetime into the appropriate datatype
            localtime = datetime.strptime(localtime, "%Y-%m-%d %H:%M:%S.%f")

            sensor = sensors[kiteid]

            if kiteid not in temperatures:
                temperatures[kiteid] = NumericData.NumericData(sensor, '°C')
            if temperatures[kiteid].add_datapoint(localtime, float(temperature)):
                sensor.supports.add('temperature')

            if kiteid not in humidities:
                humidities[kiteid] = NumericData.NumericData(sensor, '%')
            if humidities[kiteid].add_datapoint(localtime, float(humidity)):
                sensor.supports.add('humidity')

            if kiteid not in pressures:
                pressures[kiteid] = NumericData.NumericData(sensor, 'Pa')
            if pressures[kiteid].add_datapoint(localtime, float(pressure)):
                sensor.supports.add('pressure')

            if kiteid not in luminosities:
                luminosities[kiteid] = NumericData.NumericData(sensor, 'ultramillilux')
            if luminosities[kiteid].add_datapoint(localtime, float(luminosity)):
                sensor.supports.add('luminosity')

            if kiteid not in co2_values:
                co2_values[kiteid] = NumericData.NumericData(sensor, 'ppm')
            if co2_values[kiteid].add_datapoint(localtime, float(co2)):
                sensor.supports.add('co2')

            if kiteid not in sound_values:
                sound_values[kiteid] = NumericData.NumericData(sensor, 'dBA')
            if sound_values[kiteid].add_datapoint(localtime, float(sound)):
                sensor.supports.add('sound')
    return sensor_values

class CHCHSensors(Dataset):
    def __init__(self, sensor_location_csv_path='data_feeds/chchsensors/data/kite-locations.csv', sensor_values_csv_path='data_feeds/chchsensors/data/chch-sensors.csv'):
        self.sensors = load_sensors(sensor_location_csv_path)
        self.data = loadData(self.sensors, sensor_values_csv_path)
    def get_sensors(self):
        return {key: value for key, value in self.sensors.items() if len(value.supports) > 0}
    def get_feed(self, feed_key):
        return self.data.get(feed_key, None)

if __name__ == '__main__':
    # sensor_values = loadSensors('data/kite-locations.csv', 'data/chch-sensors.csv')
    # print(sensor_values)
    dataset = CHCHSensors('data/kite-locations.csv', 'data/chch-sensors.csv')
    print(dataset.get_sensors())
