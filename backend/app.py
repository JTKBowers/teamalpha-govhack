import json, datetime

from flask import Flask
from flask import request

from data_feeds.chchsensors.chchsensors import CHCHSensors
from data_feeds.airquality.hourly import HourlyAirQuality


app = Flask(__name__)

@app.after_request
def after_request(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  return response

dataset = CHCHSensors() + HourlyAirQuality()

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if type(obj) is datetime.datetime:
        serial = obj.isoformat()
        return serial
    raise TypeError ("Type {} not serializable", type(obj))

@app.route("/sensors.json")
def data_locations():
    sensor_id = request.args.get('sensor_id')
    if sensor_id is not None:
        sensor = dataset.get_sensors().get(sensor_id, None)
        if sensor is None:
            return "{}"
        return json.dumps(sensor.to_dict())
    else:
        return json.dumps({kiteid_: sensor.to_dict() for kiteid_, sensor in dataset.get_sensors().items()})

@app.route("/quantities/<datatype>.json")
def quantity_feed(datatype):
    sensor_id = request.args.get('sensor_id')
    if sensor_id is not None:
        data = dataset.get_feed(datatype).get(sensor_id, None)
        if data is None:
            return "{}"
        return json.dumps(data.render_dict(50), default=json_serial)
    else:
        return json.dumps(
            {kiteid_: location_data.render_dict(50) for kiteid_, location_data in dataset.get_feed(datatype).items()},
        default=json_serial)

@app.route("/sensors/<sensor>.json")
def sensor_feed(sensor):
    quantity_id = request.args.get('quantity_id')
    if quantity_id is not None:
        data = dataset.get_feed(quantity_id).get(sensor, None)
        if data is None:
            return "{}"
        return json.dumps(data.render_dict(50), default=json_serial)
    else:
        data = {}
        for quantity, sensors in dataset.data.items():
            if sensor in sensors:
                data[quantity] = sensors[sensor].render_dict(50)
                data['dataset_description'] = dataset.get_sensors()[sensor].dataset_description
        return json.dumps(data, default=json_serial)
if __name__ == "__main__":
    app.run(host='0.0.0.0')
