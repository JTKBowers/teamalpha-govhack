var map = L.map('sensormap').setView([-43.5321, 172.6362], 13); //chch
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
  maxZoom: 18,
  attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
    '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
    'Imagery © <a href="http://mapbox.com">Mapbox</a>',
  id: 'mapbox.streets'
}).addTo(map);


var slideshowMode = true;
// the last time setLocation was called
var lastSetLoc = new Date();

$.getJSON( serverURL + "/sensors.json", function( data ) {
//    console.log(data)
    $.each(data, function(kiteid, data) {
//      console.log(kiteid, data);
      var m = L.marker([data.lat, data.lng]);
      m.addTo(map).bindPopup(data.name).openPopup();
      /* setLocation is in dashboard.js */
      m.addEventListener('click', function(e) {
        slideshowMode=false;
        setLocation(kiteid, data.name);
      });
      data.marker = m;
    });
    // Add the graphs for the default map.
    var default_sensor = "ch_kite_0001";
    setLocation(default_sensor, data[default_sensor].name);

    // check slideshow status every 5sec
    window.setInterval(function() {
        var now = new Date();
        var elapsed = now - lastSetLoc.getTime();
        if(!slideshowMode) {
            return;
        }

        if( elapsed>20*1000 ) {
            // time for a new location!
            var keys = Object.keys(data); 
            var picked = keys[Math.floor(Math.random()*keys.length)];

            setLocation(picked, data[picked].name);
            map.openPopup(data[picked].marker.getPopup());
            lastSetLoc = new Date();
        }
    }, 5000);
});



