// Christchurch smart city dashboard
//

// perform a GET to fetch json from a URL.
// calls successfn upon success, with json data
// upon error, just logs a message to console
function grab_json( url, successfn) {
  var errfn = function(reason) {
    console.log("ERROR: grab_json("+url+"): " + reason);
  };

  var request = new XMLHttpRequest();
  request.open('GET', url, true);
  request.onload = function() {
    if (request.status >= 200 && request.status < 400) {
      // Success!
      var data = JSON.parse(request.responseText);
      successfn(data);
    } else {
      // We reached our target server, but it returned an error
      errfn("HTTP code " + request.status);
    }
  };
  request.onerror = function() { errfn("???"); }
  request.send();
};


function niceNum(v) {
    var a = Math.abs(v);
    var prec =  1 + Math.round((Math.log(a) / Math.log(10)));
    if (prec<1) prec=1;
    return v.toPrecision(prec)
}



// turn the raw data from the server API into something
// chart.js can use.
function cookData(stream) {
  var raw = stream.data;

  var maxVal = -999999;
  var minVal = 999999;
  var tot=0;
  var out = [];

  var pointslimit = 500;
  var radix = 1;
  var limit = 1;
  if (raw.length > pointslimit){
    radix = 100;
    limit = radix*pointslimit/raw.length;
  }
  for( var i=0; i<raw.length; i++) {
    var pt = raw[i];

    var val= pt[1];
    if (val<minVal) {
        minVal = val;
    }
    if (val>maxVal) {
        maxVal = val;
    }
    tot += val;

    if (i % radix <= limit)
      out.push({x: pt[0],y: val});

  }
  //console.log(raw.length, out.length);

  return {
//    name: stream.name,
    minVal: niceNum(minVal),
    maxVal: niceNum(maxVal),
    avgVal: niceNum(tot/raw.length),
    unit: stream.unit,
    data: out };
};


// the server to grab the data from
//var serverURL = "http://10.126.59.212:5000"   // Jamie
// var serverURL = "http://10.126.54.97:5000"     // Ben
var serverURL = "http://chch-dashboard.duckdns.org"     // Ben




// add a new grid row inside container
function addGridRow(container) {
    var div = document.createElement('div');
    div.className = 'row';
    container.appendChild(div);
    return div;
}


// add an empty infobox to a grid row
function addCell(row, width) {
    var div = document.createElement('div');
    div.className = 'column-' + width;
/*
    */


    row.appendChild(div);
    return div;
}





// called when user clicks on map
function setLocation(locID,prettyName) {
    // TODO: show a spinner icon while we're loading!
    var display = document.getElementById("display");

    console.log("Fetching data for " + locID + "...");
    // fetch!

    var start = new Date();
    var now;

    grab_json( serverURL + "/sensors/" + locID + ".json", function(raw) {
        now = new Date();
        console.log("success (took " + (now.getTime() - start.getTime()) + "ms)" );
        start = new Date();
        // iterate over each time series and massage into
        // a form the chart can take
        var cooked = [];
        for (var key in raw) {
          if (raw.hasOwnProperty(key)) {
            if (key == "dataset_description")
              continue
            //console.log("cooking " + key);
            var foo = cookData(raw[key]);
            foo.name = key;
            foo.iconFile = iconFromLoc(key, raw[key].unit);
            cooked.push( foo );
          }
        }
        now = new Date();
        console.log("cooked data (took " + (now.getTime() - start.getTime()) + "ms)" );
        start = now;

        // delete everything inside #display
        while(display.firstChild) {
            display.removeChild(display.firstChild);
        }
        now = new Date();
        console.log("cleared old boxes (took " + (now.getTime() - start.getTime()) + "ms)" );
        start = now;
        var row = addGridRow(display);
        var cell = addCell(row, 12);
        var box = document.createElement('div');
        box.className = 'data-desc';
        box.innerHTML = "<strong>" + prettyName + ":</strong>" + raw.dataset_description;
        cell.appendChild(box);

        // need to bunch boxes into grid rows...
        // TODO: allow for extra-wide boxes!
        var n=0;
        while(n<cooked.length) {
            var row = addGridRow(display);
            var space = 8;
            while( space>0 && n<cooked.length ) {
                if (cooked[n].data.length) {
                    //console.log("create box" + n, cooked[n]);
                    var width = 4;
                    var cell = addCell(row, width);
    var box = document.createElement('div');
    box.className = 'infobox';
    cell.appendChild(box);
                    start = new Date();
                    buildChart(box,cooked[n]);
                    now = new Date();
                    //console.log("built chart " + cooked[n].name + "(took " + (now.getTime() - start.getTime()) + "ms)" );
                    space -= width;
                } else {
                    console.log("warn - empty data: " + cooked.name);
                }
                n++;
            }
        }
    });
}


// hackery - guess a vaguely sensible icon from the location id
function iconFromLoc(quantity, unit) {
    if (/temperature/i.test(quantity)) {
        return "img/thermometer.png";
    }

    if (unit == "°C") {
        return "img/thermometer.png";
    }
    if (unit == "%") {
        return "img/humidity.png";
    }
    if (unit == "ug/m3" || unit == "ppm") {
        return "img/airquality.png";
    }
    if (unit == "Pa") {
        return "img/pressure.png";
    }
    if (unit == "m/s" || unit == "°") {
        return "img/wind.png";
    }
    if (unit == "dBA") {
        return "img/noise.png";
    }
    return "img/sunny.png";
}


// add a chart inside the given (empty) infobox
function buildChart(box,cooked) {

    var canvas = document.createElement('canvas');
    canvas.className = 'chart chart-' + cooked.name ;
    box.appendChild(canvas);


    // build up the summary below the chart
    var summary = document.createElement('div');
    summary.className = 'infosummary';

    var icon = cooked.iconFile;
    var maxtxt = '' + cooked.maxVal + '<small>'+ cooked.unit + ' max</small>';
    var mintxt = '' + cooked.minVal + '<small>' + cooked.unit + ' min</small>';
    var avgtxt = '' + cooked.avgVal + '<small>' + cooked.unit + ' avg</small>';
    summary.innerHTML = '<img src="' + icon + '" width="32px" height="32px"> ' + mintxt + ' | ' + maxtxt + ' | ' + avgtxt;
    box.appendChild(summary);

    genericChart(canvas,cooked);
}


function genericChart(ctx,cooked) {
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            datasets: [{
                //label: cooked.name + " (" + cooked.unit + ")",
                fill: false,
                tension: 0,
                stepped: true,
                data: cooked.data,
            }]
        },
        options: {
            title: {
                display: true,
                text: cooked.name,
            },
            legend: { display: false },
            scales: {
                xAxes: [{
                    ticks: {maxRotation:0},
                    type: 'time',
                    position: 'bottom',
                    time: {
                        //unit: "day",
                        displayFormats: { day: 'DD MMM HH:mm' }
                    }
                }],
                yAxes: [{
                    scaleLabel: { labelString: cooked.unit,
                        display: true }
                }]
            }
        }
    });
}

var clockDiv = document.getElementById("clock");


function showClock() {
    clockDiv.innerHTML = moment().format('MMMM Do YYYY<br/>h:mm:ss a');
}
window.setInterval(showClock,1000);
showClock();

/*var mytime=setTimeout( function() {
},1000 );
*/


