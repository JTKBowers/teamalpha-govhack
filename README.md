# Govhack 2016
## Team-alpha

This is the git repository for the team-alpha submission. Directory structure:
- All backend code & data goes in "backend/".
- All frontend code (html, js, css etc) goes in "frontend/".
- All graphics project files (.psd, etc) go in "graphics/".
- All video project files go in "video/".


## Server API:

Currently running on Jamie's laptop

eg:
 $ curl http://10.126.59.212:5000/temperature.json

replace 'temperature' with other variable names...



Have fun!
